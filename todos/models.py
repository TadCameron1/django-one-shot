from django.db import models


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now=True)


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        null=True,
        blank=True)
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(TodoList, on_delete=models.CASCADE)




# class MyModel(models.Model):
#   # This defines the fields of your model
#   name = models.CharField(max_length=100)

#   # This tells Django how to convert our model into a string
#   # when we print() it, or when the admin displays it.
#   def __str__(self):
#     return self.name
# Create your models here.
