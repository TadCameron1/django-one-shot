from django.contrib import admin

from todos.models import TodoList, TodoItem


class TodosListAdmin(admin.ModelAdmin):
    list_display = ['name', 'id']


class TodosItemAdmin(admin.ModelAdmin):
    list_display = ['task', 'due_date']


admin.site.register(TodoList, TodosListAdmin)
admin.site.register(TodoItem, TodosItemAdmin)
